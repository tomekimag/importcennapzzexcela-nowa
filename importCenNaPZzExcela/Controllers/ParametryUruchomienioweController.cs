﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace importCenNaPZzExcela.Controllers
{
    public static class ParametryUruchomienioweController
    {
        public static readonly string server;
        public static readonly string bazaDanych;
        public static readonly string uzytkownik;
        public static readonly string haslo;

        public static readonly decimal idFirmy;
        public static readonly decimal idMagazynu;
        public static readonly decimal idDokumentuMagazynowego;
        public static readonly decimal idUzytkownika;

        public static readonly decimal idKontrahentaNowa;

        public static readonly string connectionStringToModelEntities;
        public static readonly string connectionStringToAdo;

        public const int timeOut = 1200;

        //public static readonly decimal idUzytkownika;
        //public static readonly int kodKontekstu;

        static ParametryUruchomienioweController()
        {
            try
            {
#if DEBUG
                idFirmy = 1;
                idMagazynu = 1;
                idDokumentuMagazynowego = 27083;
                idUzytkownika = 3000001;

                idKontrahentaNowa = 565;

                server = "localhost\\sql2016";
                bazaDanych = "NOWASA2018";
                uzytkownik = "waproking";
                haslo = "KXF5cp7911MW501RM014";

                connectionStringToModelEntities = $@"metadata=res://*/Model.ModelNowa.csdl|res://*/Model.ModelNowa.ssdl|res://*/Model.ModelNowa.msl;provider=System.Data.SqlClient;provider connection string="";data source={server};initial catalog={bazaDanych};user id={uzytkownik};password={haslo};MultipleActiveResultSets=True;App=EntityFramework""";
                connectionStringToAdo = $@"Data Source={server};Initial Catalog={bazaDanych};User Id={uzytkownik};Password={haslo};";

#else
                String[] arguments = Environment.GetCommandLineArgs();

                if (arguments.Count() != 3) { Application.Exit(); }

                string[] param = arguments[1].ToString().Split(',');

                server = param[0];
                bazaDanych = param[1];
                uzytkownik = param[2];
                haslo = param[3];

                idKontrahentaNowa = 565;

                connectionStringToModelEntities = $@"metadata=res://*/Model.ModelNowa.csdl|res://*/Model.ModelNowa.ssdl|res://*/Model.ModelNowa.msl;provider=System.Data.SqlClient;provider connection string="";data source={server};initial catalog={bazaDanych};user id={uzytkownik};password={haslo};MultipleActiveResultSets=True;App=EntityFramework""";
                connectionStringToAdo = $@"Data Source={server};Initial Catalog={bazaDanych};User Id={uzytkownik};Password={haslo};";

                string[] param1 = arguments[2].ToString().Split(',');

                if (decimal.TryParse(param1[0], out idFirmy) == false)
                {
                    MessageBox.Show("Pobranie parematru idFirmy nie powiodło się.", "IMAG Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
                if (decimal.TryParse(param1[1], out idMagazynu) == false)
                {
                    MessageBox.Show("Pobranie parematru idMagazynu nie powiodło się.", "IMAG Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (decimal.TryParse(param1[2], out idDokumentuMagazynowego) == false)
                {
                    MessageBox.Show("Pobranie parematru idDokumentuMagazynowego nie powiodło się.", "IMAG Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                if (decimal.TryParse(param1[3], out idUzytkownika) == false)
                {
                    MessageBox.Show("Pobranie parematru idUzytkownika nie powiodło się.", "IMAG Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
#endif
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString() + " Aplikacja zostanie zamknięta.", "IMAG Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }
    }
}
