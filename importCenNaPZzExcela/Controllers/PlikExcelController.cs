﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importCenNaPZzExcela.Model;
using System.Windows.Forms;

namespace importCenNaPZzExcela.Controllers
{
    public static class PlikExcelController
    {
        public static List<PozycjaPlikuExcel> pobierzPozycjeZExcel(string sciezkaDoPliku)
        {
            
            List<PozycjaPlikuExcel> pozycje = null;

            try
            {
                var excel = new ExcelQueryFactory(sciezkaDoPliku);


                excel.AddMapping<PozycjaPlikuExcel>(x => x.kod_katalogowy, "kod katalogowy");
                excel.AddMapping<PozycjaPlikuExcel>(x => x.kod_indywidualny, "kod indywidualny");
                excel.AddMapping<PozycjaPlikuExcel>(x => x.kod_EAN, "kod EAN");
                excel.AddMapping<PozycjaPlikuExcel>(x => x.cena_zakupu_netto, "cena zakupu (netto)");

                pozycje = (from c in excel.WorksheetRange<PozycjaPlikuExcel>("A1", "D10000", "Arkusz1")
                           where c.kod_katalogowy != ""
                           select c).ToList();

                foreach (var p in pozycje)
                {
                    p.statusPozycji = "Nierozpoznana";
                    p.idPozDokMagazynowego = 0;
                }

            }
            catch(Exception e)
            {
                MessageBox.Show($@"Błąd czytania pliku Excel:{e.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return pozycje;
        }

    }
}
