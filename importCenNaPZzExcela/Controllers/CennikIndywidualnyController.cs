﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importCenNaPZzExcela.Model;

namespace importCenNaPZzExcela.Controllers
{
    public static class CennikIndywidualnyController
    {
        public static CennikIndywidualnyModel ZwrocCennikIndywidualny(string indeks)
        {
            CennikIndywidualnyModel cennikIndywidualny;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                cennikIndywidualny = (from dm in entity.DOKUMENT_MAGAZYNOWY
                                          join k in entity.KONTRAHENT on dm.ID_KONTRAHENTA equals k.ID_KONTRAHENTA
                                          join ci in entity.CENNIK_INDYWIDUALNY on k.ID_GRUPY equals ci.ID_GRUPY
                                          join pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO on dm.ID_DOK_MAGAZYNOWEGO equals pdm.ID_DOK_MAGAZYNOWEGO
                                          where dm.ID_DOK_MAGAZYNOWEGO == ParametryUruchomienioweController.idDokumentuMagazynowego
                                          && ci.INDEKS == indeks
                                          && ci.ID_ARTYKULU == pdm.ID_ARTYKULU
                                          select new CennikIndywidualnyModel()
                                          {
                                              ID_ARTYKULU = ci.ID_ARTYKULU,
                                              ID_POZ_DOK_MAG = pdm.ID_POZ_DOK_MAG
                                          }).FirstOrDefault();
            }
            return cennikIndywidualny;
        }
    }
}
