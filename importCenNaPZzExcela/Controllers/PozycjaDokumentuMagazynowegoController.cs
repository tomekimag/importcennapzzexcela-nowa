﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importCenNaPZzExcela.Model;
using System.Data.Entity.Core.Objects;
using System.Windows.Forms;

namespace importCenNaPZzExcela.Controllers
{
    public static class PozycjaDokumentuMagazynowegoController
    {
        public static List<PozycjaDokumentuMagazynowegoModel> PobierzPozycjeDokumentuMagazynowegoPZ(List<PozycjaPlikuExcel> pozycjeZExcela)
        {
            List<PozycjaDokumentuMagazynowegoModel> pozycjeDokMagazynowego;

            List<string> listaKodowKreskowych = (from pzpe in pozycjeZExcela
                                                 select pzpe.kod_EAN.ToString()).ToList();

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjeDokMagazynowego = (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                          join a in entity.ARTYKUL on pdm.ID_ARTYKULU equals a.ID_ARTYKULU
                                          join kk in entity.KOD_KRESKOWY on a.ID_ARTYKULU equals kk.ID_ARTYKULU
                                          where pdm.ID_DOK_MAGAZYNOWEGO == ParametryUruchomienioweController.idDokumentuMagazynowego
                                          && listaKodowKreskowych.Contains(kk.KOD_KRESKOWY1)
                                          select new PozycjaDokumentuMagazynowegoModel()
                                          {
                                              ID_ARTYKULU = pdm.ID_ARTYKULU,
                                              ID_POZ_DOK_MAG = pdm.ID_POZ_DOK_MAG,
                                              ID_DOK_MAGAZYNOWEGO = pdm.ID_DOK_MAGAZYNOWEGO,
                                              ILOSC = pdm.ILOSC,
                                              CENA_NETTO = pdm.CENA_NETTO,
                                              CENA_BRUTTO = pdm.CENA_BRUTTO,
                                              KOD_KRESKOWY1 = kk.KOD_KRESKOWY1,
                                              INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY
                                          }).ToList();
            }

            return pozycjeDokMagazynowego;
        }

        public static PozycjaDokumentuMagazynowegoModel PobierzPozycjeDokumentuMagazynowegoPZPoIndeksieKatalogowym(string kod_katalogowy)
        {
            PozycjaDokumentuMagazynowegoModel pozycjaDokMagazynowego;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjaDokMagazynowego = (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                          join a in entity.ARTYKUL on pdm.ID_ARTYKULU equals a.ID_ARTYKULU
                                          where pdm.ID_DOK_MAGAZYNOWEGO == ParametryUruchomienioweController.idDokumentuMagazynowego
                                          && a.INDEKS_KATALOGOWY == kod_katalogowy
                                          select new PozycjaDokumentuMagazynowegoModel()
                                          {
                                              ID_ARTYKULU = pdm.ID_ARTYKULU,
                                              ID_POZ_DOK_MAG = pdm.ID_POZ_DOK_MAG,
                                              ID_DOK_MAGAZYNOWEGO = pdm.ID_DOK_MAGAZYNOWEGO,
                                              ILOSC = pdm.ILOSC,
                                              CENA_NETTO = pdm.CENA_NETTO,
                                              CENA_BRUTTO = pdm.CENA_BRUTTO,
                                              KOD_KRESKOWY1 = a.KOD_KRESKOWY,
                                              INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY
                                          }).FirstOrDefault();
            }

            return pozycjaDokMagazynowego;
        }

        public static void ZapiszPozycjeDokumentuMagazynowegoPZ(PozycjaPlikuExcel pozycjaZExcela, NOWASA2018Entities entity)
        {
            if (pozycjaZExcela.statusPozycji != "Ok") return;

            POZYCJA_DOKUMENTU_MAGAZYNOWEGO pozycja = (from pdm in entity.POZYCJA_DOKUMENTU_MAGAZYNOWEGO
                                                      where pdm.ID_POZ_DOK_MAG == pozycjaZExcela.idPozDokMagazynowego
                                                      select pdm).FirstOrDefault();

            if (pozycja != null)
            {
                pozycja.CENA_NETTO = pozycjaZExcela.cena_zakupu_netto;
                pozycja.CENA_BRUTTO = pozycjaZExcela.cena_zakupu_netto * (decimal.Parse(pozycja.KOD_VAT.Trim().ToString() + 1) / 100);
            }

            entity.SaveChanges();
        }
    }
}
