﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using importCenNaPZzExcela.Model;
using System.Data.Entity.Core.Objects;
using System.Windows.Forms;

namespace importCenNaPZzExcela.Controllers
{
    public static class DokumentMagazynowyController
    {
        public static string ZapisDokumentuMagazynowegoPZ(NOWASA2018Entities entity)
        {
            string wynik = string.Empty;
            try
            {

                ObjectParameter suma_netto = new ObjectParameter("suma_netto", typeof(decimal));
                suma_netto.Value = 9999999.0001m;

                ObjectParameter suma_brutto = new ObjectParameter("suma_brutto", typeof(decimal));
                suma_brutto.Value = 9999999.0001m;

                entity.JL_SumujDokumentMagazynowy_Server(int.Parse(ParametryUruchomienioweController.idDokumentuMagazynowego.ToString()), "PZ", "Netto", 1, suma_netto, suma_brutto);

                decimal suma_netto_;
                decimal suma_brutto_;
                suma_netto_ = decimal.Parse(suma_netto.Value.ToString());
                suma_brutto_ = decimal.Parse(suma_brutto.Value.ToString());

                DOKUMENT_MAGAZYNOWY dokument = (from dm in entity.DOKUMENT_MAGAZYNOWY
                                                where dm.ID_DOK_MAGAZYNOWEGO == ParametryUruchomienioweController.idDokumentuMagazynowego
                                                select dm).FirstOrDefault();

                entity.JL_ZatwierdzZmianyDokumentuMagazynowego(Convert.ToInt32(ParametryUruchomienioweController.idDokumentuMagazynowego), null, dokument.RODZAJ_DOKUMENTU, dokument.DATA, dokument.NUMER,
                    Convert.ToInt32(dokument.ID_KONTRAHENTA), suma_netto_, suma_brutto_, "fifo", dokument.BRUTTO_NETTO,
                    0, 0, 1, ParametryUruchomienioweController.idFirmy, dokument.ID_MAGAZYNU,
                    ParametryUruchomienioweController.idUzytkownika, 0, dokument.UWAGI, 1, 0, "",
                    0, dokument.DATA_VAT);
            }
            catch (Exception e)
            {
                wynik = $@"{e.Message} {e.InnerException.Message}";
                //MessageBox.Show($@"{e.Message} {e.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return wynik;
        
        }
    }
}
