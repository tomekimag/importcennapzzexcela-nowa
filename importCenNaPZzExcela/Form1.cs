﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using importCenNaPZzExcela.Controllers;
using importCenNaPZzExcela.Model;

namespace importCenNaPZzExcela
{
    public partial class Form1 : Form
    {
        private List<PozycjaPlikuExcel> pozycje;

        public Form1()
        {
            InitializeComponent();
        }

        private void button_zaladujPlikExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pozycje = PlikExcelController.pobierzPozycjeZExcel(openFileDialog1.FileName);

                List<PozycjaDokumentuMagazynowegoModel> listaPozycjiDokMagazynowego = PozycjaDokumentuMagazynowegoController.PobierzPozycjeDokumentuMagazynowegoPZ(pozycje);

                foreach(var p in pozycje)
                {
                    var pdm = listaPozycjiDokMagazynowego.Where(x => (p.kod_EAN.ToString() != "" && x.KOD_KRESKOWY1 == p.kod_EAN.ToString())
                    || (p.kod_katalogowy != "" && x.INDEKS_KATALOGOWY == p.kod_katalogowy)).FirstOrDefault();
                    if (pdm != null)
                    {
                        p.idPozDokMagazynowego = pdm.ID_POZ_DOK_MAG;
                        p.statusPozycji = "Ok";
                    }
                    else
                    {
                        PozycjaDokumentuMagazynowegoModel pozycjaDokMagazynowego = PozycjaDokumentuMagazynowegoController.PobierzPozycjeDokumentuMagazynowegoPZPoIndeksieKatalogowym(p.kod_katalogowy);

                        if ((p.kod_katalogowy != string.Empty) && (pozycjaDokMagazynowego != null))
                        {
                            p.idPozDokMagazynowego = pozycjaDokMagazynowego.ID_POZ_DOK_MAG;
                            p.statusPozycji = "Ok";
                        }
                        else if (p.kod_indywidualny != string.Empty)
                        {
                            CennikIndywidualnyModel cennikIndywidualny = CennikIndywidualnyController.ZwrocCennikIndywidualny(p.kod_indywidualny);
                            if (cennikIndywidualny != null)
                            {
                                p.idPozDokMagazynowego = cennikIndywidualny.ID_POZ_DOK_MAG;
                                p.statusPozycji = "Ok";
                            }
                        }
                    }
                }

                dataGridView_cenyNaPZ.DataSource = pozycje;
            }
        }

        private void dataGridView_cenyNaPZ_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "kod_katalogowy":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "kod katalogowy";
                    break;
                case "kod_indywidualny":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "kod indywidualny";
                    break;
                case "kod_EAN":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "kod EAN";
                    break;
                case "cena_zakupu_netto":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "cena zakupu (netto)";
                    break;
                case "statusPozycji":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "Czy poz. rozpoznana";
                    break;                  
                default:
                    e.Column.Visible = false;
                    break;
            }
        }

        private void dataGridView_cenyNaPZ_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_zapisz_Click(object sender, EventArgs e)
        {
            if (pozycje == null) { MessageBox.Show($@"Zaczytaj plik Excel.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

            //PozycjaPlikuExcel pozycja = pozycje.Where(x => x.statusPozycji != "Ok").FirstOrDefault();

            //if (pozycja != null) { MessageBox.Show($@"Istnieją nierozpoznane pozycje.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }

            List<PozycjaPlikuExcel> listaPowiazanych = (from p in pozycje
                                                        where p.idPozDokMagazynowego != 0
                                                        select p).ToList();
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {

                foreach (var p in listaPowiazanych)
                {
                    PozycjaDokumentuMagazynowegoController.ZapiszPozycjeDokumentuMagazynowegoPZ(p, entity);
                }
                string wynik = DokumentMagazynowyController.ZapisDokumentuMagazynowegoPZ(entity);

                if (wynik != string.Empty)
                {
                    MessageBox.Show($@"{wynik}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($@"Zmiany na dokumencie zostały wprowadzone. Moduł zostanie zamknięty.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }            
        }
    }
}
