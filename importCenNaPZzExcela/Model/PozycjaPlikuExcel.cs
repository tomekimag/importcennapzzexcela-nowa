﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importCenNaPZzExcela.Model
{
    public class PozycjaPlikuExcel
    {
        public string statusPozycji { get; set; }
        public string kod_katalogowy { get; set; }  //kod katalogowy
        public string kod_indywidualny { get; set; }    //kod indywidualny
        public long kod_EAN { get; set; }         //kod EAN
        public decimal cena_zakupu_netto { get; set; }  //cena zakupu (netto)
        public decimal idPozDokMagazynowego { get; set; }
    }
}
