﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importCenNaPZzExcela.Model
{
    public class CennikIndywidualnyModel
    {
        public decimal ID_ARTYKULU { get; set; }

        /// <summary>
        /// Z tabeli pozycja dokumentu magazynowego.
        /// </summary>
        public decimal ID_POZ_DOK_MAG { get; set; }
    }
}
