﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace importCenNaPZzExcela.Model
{
    public class PozycjaDokumentuMagazynowegoModel
    {
        public decimal ID_POZ_DOK_MAG { get; set; }
        public Nullable<decimal> ID_DOK_MAGAZYNOWEGO { get; set; }
        public Nullable<decimal> ID_ARTYKULU { get; set; }
        public decimal ILOSC { get; set; }
        public Nullable<decimal> CENA_NETTO { get; set; }
        public Nullable<decimal> CENA_BRUTTO { get; set; }

        /// <summary>
        /// Z tabeli kod kreskowy
        /// </summary>
        public string KOD_KRESKOWY1 { get; set; }

        /// <summary>
        /// Z tabeli artykul
        /// </summary>
        public string INDEKS_KATALOGOWY { get; set; }
    }
}
