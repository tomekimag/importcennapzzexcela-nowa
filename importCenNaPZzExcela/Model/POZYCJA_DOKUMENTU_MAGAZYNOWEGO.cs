//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace importCenNaPZzExcela.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class POZYCJA_DOKUMENTU_MAGAZYNOWEGO
    {
        public decimal ID_POZ_DOK_MAG { get; set; }
        public Nullable<decimal> ID_DOK_MAGAZYNOWEGO { get; set; }
        public Nullable<decimal> ID_DOK_HANDLOWEGO { get; set; }
        public Nullable<decimal> ID_ARTYKULU { get; set; }
        public Nullable<decimal> ID_POZ_ORYGINALNEJ { get; set; }
        public Nullable<decimal> ID_POZ_KORYGOWANEJ { get; set; }
        public Nullable<decimal> ID_OST_KOREKTY { get; set; }
        public Nullable<decimal> ID_POW_KOREKTY { get; set; }
        public Nullable<byte> POZ_KOREKTY { get; set; }
        public string KOD_VAT { get; set; }
        public decimal ILOSC { get; set; }
        public decimal WYDANO { get; set; }
        public decimal ZAREZERWOWANO { get; set; }
        public string RODZAJ_POZYCJI { get; set; }
        public Nullable<byte> POZ_PRZYCHODU_OK { get; set; }
        public Nullable<int> DATA { get; set; }
        public Nullable<decimal> CENA_NETTO { get; set; }
        public Nullable<decimal> CENA_BRUTTO { get; set; }
        public Nullable<decimal> CENA_NETTO_WAL { get; set; }
        public Nullable<decimal> CENA_BRUTTO_WAL { get; set; }
        public string ZNACZNIK_CENY { get; set; }
        public string OPIS_CENY { get; set; }
        public string JEDNOSTKA { get; set; }
        public Nullable<decimal> PRZELICZNIK { get; set; }
        public Nullable<decimal> RABAT { get; set; }
        public Nullable<decimal> RABAT2 { get; set; }
        public Nullable<decimal> RABAT2_ZB { get; set; }
        public Nullable<byte> FLAGA_STANU { get; set; }
        public string RODZAJ_ARTYKULU { get; set; }
        public Nullable<decimal> OPAKOWANIA_WYDANO { get; set; }
        public Nullable<decimal> OPAKOWANIA_PRZYJETO { get; set; }
        public string OPIS { get; set; }
        public Nullable<decimal> ID_POZ_ZAM { get; set; }
        public string NR_PACZKI { get; set; }
        public byte TRYBREJESTRACJI { get; set; }
        public Nullable<decimal> ID_POZ_PRZYCH { get; set; }
        public byte POZ_WYB_DOSTAWY { get; set; }
        public string NR_SERII { get; set; }
        public Nullable<System.Guid> GUID_POZ_DOSTAWY_MM { get; set; }
        public Nullable<System.Guid> GUID_POZ_DOK_MAG { get; set; }
        public int DATA_WAZNOSCI { get; set; }
        public Nullable<decimal> ID_POZ_ZLECENIA { get; set; }
        public string KOD_VAT_UE { get; set; }
        public Nullable<decimal> CENA_NETTO_BEZ_KOSZTU { get; set; }
        public Nullable<decimal> CENA_BRUTTO_BEZ_KOSZTU { get; set; }
        public Nullable<byte> I_PODLEGA_DEKLARACJI { get; set; }
        public string I_KOD_CN { get; set; }
        public string I_KOD_TRANSPORTU { get; set; }
        public string I_KOD_TRANSAKCJI { get; set; }
        public string I_KOD_DOSTAWY { get; set; }
        public string I_KOD_KRAJU_PRZEZ_WYS { get; set; }
        public string I_KOD_KRAJU_POCH { get; set; }
        public Nullable<decimal> I_MASA_NETTO { get; set; }
        public Nullable<decimal> ID_POZ_OBROTOWEJ { get; set; }
        public Nullable<decimal> ID_POZ_ZAM_ZAL { get; set; }
        public Nullable<byte> AKT_CEN_PRZY_DOSTAWIE { get; set; }
        public Nullable<decimal> CN_SPRZ_PZ { get; set; }
        public Nullable<decimal> CB_SPRZ_PZ { get; set; }
        public Nullable<decimal> CENA_N_KGO { get; set; }
        public Nullable<decimal> CENA_B_KGO { get; set; }
        public Nullable<decimal> CZB_MARZA { get; set; }
        public Nullable<byte> AKCYZA { get; set; }
        public Nullable<decimal> ID_DOK_KORYGUJACEGO_ZBIORCZO { get; set; }
        public string I_KOD_KONTRAHENTA_ZAGR { get; set; }
        public string POLE1 { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE4 { get; set; }
        public string POLE5 { get; set; }
        public string POLE6 { get; set; }
        public string POLE7 { get; set; }
        public string POLE8 { get; set; }
        public string POLE9 { get; set; }
        public string POLE10 { get; set; }
        public Nullable<decimal> ID_POZYCJI_OFERTY { get; set; }
    }
}
