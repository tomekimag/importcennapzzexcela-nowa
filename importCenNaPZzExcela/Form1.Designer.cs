﻿namespace importCenNaPZzExcela
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_zapisz = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.dataGridView_cenyNaPZ = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_zaladujPlikExcel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cenyNaPZ)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_zapisz);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 428);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(999, 79);
            this.panel1.TabIndex = 0;
            // 
            // button_zapisz
            // 
            this.button_zapisz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zapisz.Location = new System.Drawing.Point(821, 27);
            this.button_zapisz.Name = "button_zapisz";
            this.button_zapisz.Size = new System.Drawing.Size(75, 23);
            this.button_zapisz.TabIndex = 1;
            this.button_zapisz.Text = "Zapisz";
            this.button_zapisz.UseVisualStyleBackColor = true;
            this.button_zapisz.Click += new System.EventHandler(this.button_zapisz_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(902, 27);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // dataGridView_cenyNaPZ
            // 
            this.dataGridView_cenyNaPZ.AllowUserToAddRows = false;
            this.dataGridView_cenyNaPZ.AllowUserToDeleteRows = false;
            this.dataGridView_cenyNaPZ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_cenyNaPZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_cenyNaPZ.Location = new System.Drawing.Point(0, 63);
            this.dataGridView_cenyNaPZ.Name = "dataGridView_cenyNaPZ";
            this.dataGridView_cenyNaPZ.ReadOnly = true;
            this.dataGridView_cenyNaPZ.Size = new System.Drawing.Size(999, 365);
            this.dataGridView_cenyNaPZ.TabIndex = 1;
            this.dataGridView_cenyNaPZ.DataSourceChanged += new System.EventHandler(this.dataGridView_cenyNaPZ_DataSourceChanged);
            this.dataGridView_cenyNaPZ.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_cenyNaPZ_ColumnAdded);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_zaladujPlikExcel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(999, 63);
            this.panel2.TabIndex = 2;
            // 
            // button_zaladujPlikExcel
            // 
            this.button_zaladujPlikExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zaladujPlikExcel.Location = new System.Drawing.Point(803, 20);
            this.button_zaladujPlikExcel.Name = "button_zaladujPlikExcel";
            this.button_zaladujPlikExcel.Size = new System.Drawing.Size(174, 23);
            this.button_zaladujPlikExcel.TabIndex = 0;
            this.button_zaladujPlikExcel.Text = "Załaduj plik EXCEL";
            this.button_zaladujPlikExcel.UseVisualStyleBackColor = true;
            this.button_zaladujPlikExcel.Click += new System.EventHandler(this.button_zaladujPlikExcel_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 507);
            this.Controls.Add(this.dataGridView_cenyNaPZ);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Import cen na PZ";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cenyNaPZ)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView_cenyNaPZ;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_zaladujPlikExcel;
        private System.Windows.Forms.Button button_zapisz;
        private System.Windows.Forms.Button button_anuluj;
    }
}

